import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { RestApiProvider } from '../../providers/rest-api/rest-api';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [RestApiProvider]
})
export class HomePage {

  constructor(
    public navCtrl: NavController,
    public rest: RestApiProvider
  ) {}

  countries: string[];
  errorMessage: string;
  descending: boolean = false;
  order: number;
  column: string = 'name';
  terms: string = ''

  getCountries(){
    this.rest.getCountries().subscribe(
      countries => {
        console.log(countries)
        this.countries = countries
      },
      error => this.errorMessage
    )
  }

  ionViewDidLoad() {
    this.getCountries();
  }

  sort(){
    this.descending = !this.descending;
    this.order = this.descending ? 1 : -1;
  }

}
